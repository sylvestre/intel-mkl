intel-mkl (2019.0.117-2) unstable; urgency=medium

  * Export HOME=/tmp/ to fix FTBFS due to the failure that rpm would
    attempt to store GPG key used for verifying rpm packages under
    /sbuild-nonexistent/.rpmdb directory without a proper HOME.

 -- Mo Zhou <cdluminate@gmail.com>  Wed, 26 Sep 2018 02:46:51 +0000

intel-mkl (2019.0.117-1) unstable; urgency=medium

  * New upstream release 2019.0.117 (Sept 2018). (Closes: #909507)
  * Update control.py for the 2019.0.117 release.
  * Verify RPM signature with the given public key before extraction.
  * Fix wrong shell redirection in libmkl-rt's postinst. (Closes: #908240)
  * Fix wrong shell redirection in libmkl-dev's postinst. (Closes: #908244)
  * Update README.Debian, and merge TODO into it.
  * Monitoring https://pypi.org/project/mkl/ for updates via watch.
    Note that pypi is not where the real source package comes from.
  * Remove bpo.patch and provide control.bpo.py which automatically
    converts control.py to support python3.5 (for stretch-backports).
  * Copyright update: ISSL updated to April 2018 Version.
  * Autopkgtest: Add a simple level3 blas (dgemm) sanity test.
  * Bump Standards-Version to 4.2.1 (no change).

  [ Localization Updates ]
  * Update Simplified Chinese translation.
  * Add French translation for debconf messages. (Closes: #906732)
    Thanks to jean-pierre giraud <jean-pierregiraud@neuf.fr>
  * Add Dutch translation for debconf messages. (Closes: #906947)
    Thanks to Frans Spiesschaert <Frans.Spiesschaert@yucom.be>
  * Add German translation for debconf messages. (Closes: #908926)
    Thanks to Helge Kreutzmann <debian@helgefjell.de>
  * Add Portuguese translation for debconf messages. (Closes:#909528)
    Thanks to Traduz PT <traduz@debianpt.org>

 -- Mo Zhou <cdluminate@gmail.com>  Tue, 25 Sep 2018 06:11:34 +0000

intel-mkl (2018.3.222-3) unstable; urgency=medium

  * Update Depends, Recommends and Suggests fields, and strip unnecessary
    explicit architecture specifications (:amd64, :i386) from these fields.

 -- Mo Zhou <cdluminate@gmail.com>  Tue, 07 Aug 2018 07:06:58 +0000

intel-mkl (2018.3.222-2) unstable; urgency=medium

  * Fix FTBFS (UnicodeDecodeError) in C locale. (Closes: #904434)
  * Explicitly explain why this package is non-free.
  * Mark the source package as XS-Autobuild: yes.
  * Move the cross-arch (i386) dependencies from Depends to Suggests in
    order to unblock migration to testing.
  * Provide patch debian/bpo.patch to support Python3.5, for backporters.
    This patch is not enabled by default. (Closes: #904320)
  * Make the checksum target non-mandatory during build. (Closes: #904319)
  * Change libmkl-locale's Architecture to (amd64, i386) to prevent
    build on any other non-x86 architectures.
  * Make the traversal order over *.rpm packages during extraction fixed.
  * Bump Standards-Version to 4.2.0 (no changes).

 -- Mo Zhou <cdluminate@gmail.com>  Fri, 03 Aug 2018 15:11:56 +0000

intel-mkl (2018.3.222-1) unstable; urgency=low

  * Initial release. (Closes: #895881)
  * Many thanks to Sébastien Villemot for helpful advices.

 -- Mo Zhou <cdluminate@gmail.com>  Mon, 18 Jun 2018 11:57:36 +0000
